import axios from 'axios';

const protocol = process.env.REACT_APP_PROTOCOL;
const apiHost = process.env.REACT_APP_API_HOST;
const apiPort = process.env.REACT_APP_API_PORT;

const basePath = `${protocol}://${apiHost}:${apiPort}`;

export const fetchMessage = () => {
  // Make a request for a user with a given ID
  return axios
    .get(`${basePath}/message/get`);
};
