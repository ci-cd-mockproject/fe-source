import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";

import { fetchMessage } from "./services/message.services";

function App() {
  const [data, setData] = useState();

  useEffect(() => {
    async function fetchData() {
      let message = await fetchMessage();
      console.log(message);
      setData(message.data);
    }
    fetchData();
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>{data}</p>
      </header>
    </div>
  );
}

export default App;
